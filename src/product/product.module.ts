import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { LayoutModule } from 'src/layout/layout.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

const router = [
  { path: '', component: ProductComponent }
];

@NgModule({
  declarations: [ProductComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    RouterModule.forChild(router)
  ]
})
export class ProductModule { }
