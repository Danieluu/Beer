import { Product } from 'src/model/product';

export interface IDictionary<T> {
    [key: string]: T
};

export const MockProduct: Product[] = [
    {
        id: '1',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '2',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '3',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '4',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '5',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '6',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '7',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '8',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '9',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '10',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '11',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    },
    {
        id: '12',
        chineseName: "拉格",
        englishName: 'Lager',
        chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
        englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
        location: "台北",
        imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
    }
]

// export const MockProduct: Product[] = [
//     {
//         id: '1',
//         chineseName: "拉格",
//         englishName: 'Lager',
//         chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
//         englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
//         location: "台北",
//         imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
//     },
//     {
//         id: '2',
//         chineseName: "拉格",
//         englishName: 'Lager',
//         chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
//         englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
//         location: "台北",
//         imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
//     },
//     {
//         id: '3',
//         chineseName: "拉格",
//         englishName: 'Lager',
//         chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
//         englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
//         location: "台北",
//         imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
//     },
//     {
//         id: '4',
//         chineseName: "拉格",
//         englishName: 'Lager',
//         chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
//         englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
//         location: "台北",
//         imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
//     },
//     {
//         id: '5',
//         chineseName: "拉格",
//         englishName: 'Lager',
//         chineseDescription: '\"拉格\"是一種低溫條件下的啤酒。可以是淡黃色，琥珀色或深色。淡啤酒是最廣泛消費和商業化的啤酒風格\r\n',
//         englishDescription: 'Lager is a type of beer confitioned at low temperatures. Lagers can be yellow pale,amber,or dark. Pale lager is the most widely consumed and commercially available style of beer.\r\n',
//         location: "台北",
//         imageUrl: 'https://cdn.gearpatrol.com/wp-content/uploads/2016/04/Lager-Beer-Gear-Patrol-Sixpoint.jpg'
//     }
// ]