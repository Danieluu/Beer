export interface Product {
    id: string;
    chineseName: string;
    englishName: string;
    chineseDescription: string;
    englishDescription: string;
    location: string;
    imageUrl: string;
};