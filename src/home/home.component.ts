import { Component } from '@angular/core';
import { Product } from 'src/model/product';
import { MockProduct } from 'src/mock/product';
declare var $: any;

@Component({
  templateUrl: './home.html',
  styleUrls: ["./home.css"]
})
export class HomeComponent {
  selectProduct: Product = {} as Product;
  modalProduct: Product = {} as Product;
  products: Product[] = MockProduct;
  popoverContent: string = "";

  constructor() { }

  ngOnInit() {
  };

  ngAfterViewInit() {
    $('.product-list').slick({
      slidesToShow: 6,
      slidesToScroll: 6
    });
  };

  closeCollapse(ele:HTMLElement) {
    $('#'+ele.id).collapse('hide');
  };

  selected(product: Product, index: string) {
    var $collapse = $('#' + index);
    if (this.selectProduct.id === product.id) {
      this.selectProduct = {} as Product;
      $collapse.collapse('hide');
    } else if($collapse.hasClass('show')) {
      this.selectProduct = product;
    } else {
      this.selectProduct = product;
      $collapse.collapse('show');
    }
  };
}
