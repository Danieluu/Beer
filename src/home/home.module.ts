import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { HomeComponent } from './home.component';
import { LayoutModule } from 'src/layout/layout.module';
import { RouterModule } from '@angular/router';

const router = [
  {path: '', component: HomeComponent}
]

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    LayoutModule,
    FormsModule,
    RouterModule.forChild(router)
  ]
})
export class HomeModule { }
